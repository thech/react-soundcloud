import Stream from '../../../src/components/Stream';
import { shallow } from 'enzyme';
import { Track } from '../../../src/components/entities';
import { expect } from 'chai';

describe('Stream', () => {
    const props = {
        tracks: [
            new Track('x'),
            new Track('y'),
        ]
    };

    it('shows two elements', () => {
        const element = shallow(Stream(props.tracks));

        expect(element.find('.track')).to.have.length(2);
    });

});