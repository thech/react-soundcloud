const path = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const webpack = require('webpack');

module.exports = {
    // Enable source maps. For production use "source-map"
    devtool: 'cheap-module-source-map',
    entry: [
        // activate HMR for React
        'react-hot-loader/patch',
        //Bundle the client and connect to endpoint
        'webpack-dev-server/client?http://localhost:3000',
        //Interface for HMR, do not reload on error
        'webpack/hot/only-dev-server',
        //Entry point
        './src/index.tsx'
    ],
    output: {
        filename: '[name].bundle.js',
        path: path.join(__dirname, 'build'),
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', 'jsx', '.html']
    },
    devServer: {
        host: 'localhost',
        port: '3000',
        hot: true,
        // respond to 404s with index.html
        historyApiFallback: true,
        inline: true,
        contentBase: path.join(__dirname, 'build'),
        //publicPath: '/'
    },
    module: {
        loaders: [{
            test: /\.ts(x?)$/,
            exclude: /node-modules/,
            loaders: ['babel-loader', 'awesome-typescript-loader'],
        }, {
            test: /\.js$/,
            enforce: 'pre',
            loader: 'source-map-loader',
        }],
    },
    plugins: [
        new WebpackNotifierPlugin({ alwaysNotify: true }),
        //Enable HMR globally
        new webpack.HotModuleReplacementPlugin(),
        //Readable module names
        new webpack.NamedModulesPlugin(),
        //ATLoader checker
        new CheckerPlugin(),
        //For html
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src/index.html'),
            inject: 'body',
            hash: true,
        }),
    ]
};