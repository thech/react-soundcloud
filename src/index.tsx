import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './components/App';
declare const module: { hot: any };



ReactDOM.render(
    <AppContainer>
        <App />
    </AppContainer>,
    document.getElementById('app'),
);

module.hot.accept();
