import * as React from 'react';
import { Track } from './entities';

export default function Stream(tracks: Track[]) {
    return (
        <div>
            {
                tracks.map((track, key) => {
                    return <div className="track" key={key}>
                        {track.title}
                    </div>;
                })
            }
        </div>
    )
}