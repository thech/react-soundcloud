import * as React from 'react';
import { Track } from './entities';
import Stream from './Stream';

const tracks = [
    new Track('some okay rack'),
    new Track('some bad track'),
];

export default class App extends React.Component<{}, {}> {
    constructor() {
        super();
    }
    render() {
        return(
            Stream(tracks)
        )
    }
}
