const webpackConfig = require('./webpack.config');

module.exports = function(config) {
    config.set({
        autoWatch: true,
        basePath: "",
        browsers: ["PhantomJS"],
        colors: true,
        exclude: [],
        files: [
            "test/**/*.tsx",
            "test/**/*.ts"
        ],
        frameworks: ["mocha", "chai", "sinon"],
        logLevel: config.LOG_INFO,
        plugins: [
            "karma-*"
        ],
        port: 9876,
        preprocessors: {
            "test/**/*.tsx": ["webpack"],
            "test/**/*.ts": ["webpack"]
        },
        reporters: ["mocha"],
        singleRun: false,
        webpack: {
            module: webpackConfig.module,
            resolve: webpackConfig.resolve
        }
    })
};